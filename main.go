package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/color"
)

func main() {

	lines := toLines(readFile("target.txt"))
	greeter(lines)
	fmt.Print(lines)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func readFile(filename string) string {
	data, err := ioutil.ReadFile(filename)
	check(err)
	return string(data)
}

func toLines(data string) []string {
	lines := make([]string, lineCounter(data))
	index := 0
	var line string
	for _, d := range data {
		str := string(d)
		if d != 10 {
			line += str
		} else {
			lines[index] = line
			index++
			line = ""
		}
		lines[index] = line
	}
	return lines
}

func lineCounter(data string) (lenght uint64) {
	lenght = 1
	for _, d := range data {
		if d == 10 {
			lenght++
		}
	}
	return
}

func greeter(lines []string) {
	var la string
	langs := map[string][]string{
		"EN": {
			"w",
		},
		"TR": {
			"Girdiğiniz değer uyuşmuyor. Lütfen istenilen şekilde girdi yapın !",
			"Kelime Bulucu'ya hoşgeldiniz. Bu programdaki amaç kelimleri satır satır arayıp aranan kelimeyi istenilen satırda renkli bir şekilde göstermektir.\nÖrnek:\nKelimeleri girin : Selamlar\n\n\n\n",
			"1) Tamamdır anladım.\n2) Hayır çıkmak istiyorum.",
			"Peki... Sen bilirisin.",
			"Pekala şimdi başlıyoruz...",
			"Kelimleri girin :",
			"Aranan kelimeler bulunamadı !",
			"Kelimenin bulunduğu satır sayısı",
			"Gösterilen",
			"-Daha fazla sonuç için Enter \t -Yeni kelime için 1 \t -Çıkmak için CTRL+C",
			"Çıkılıyor",
		},
	}
	for {
		fmt.Println("Choose your lang: EN , TR")
		lang := inpuStr()
		if _, ok := langs[lang]; ok {
			la = lang
			break
		} else {
			fmt.Println("This lang doesnt supported. Please ensure your typing !")
		}
	}
	k1 := langs[la]
	fmt.Println(k1[1])
	fmt.Println(k1[2])
	input := inputInt()
	if input == 1 {
		fmt.Println(k1[4])
		time.Sleep(time.Second * 2)
		startSearch(k1, lines)
	} else {
		fmt.Println(k1[3])
		fmt.Println(k1[10])
		time.Sleep(time.Second * 3)
		os.Exit(3)
	}
}

func inpuStr() (input string) {
	scanner := bufio.NewScanner(os.Stdin)
	for i := 0; i < 1 && scanner.Scan(); i++ {
		input = scanner.Text()
	}
	return
}

func inputInt() (input int) {
	var text string
	scanner := bufio.NewScanner(os.Stdin)
	for c := 0; c < 1 && scanner.Scan(); c++ {
		text = scanner.Text()
	}
	input64, _ := strconv.ParseInt(text, 10, 32)
	input = int(input64)
	return
}

func startSearch(langArgs []string, lines []string) {
	green := color.New(color.Bold, color.FgGreen).SprintFunc()
	for {
		fmt.Println(langArgs[5])
		input := inpuStr()
		var resultCounter int
		for _, line := range lines {
			if strings.Contains(line, input) {
				resultCounter++
			}
		}
		if resultCounter == 0 {
			color.Red(langArgs[6])
		}
		results := make([]string, resultCounter)
		var index int

		for _, line := range lines {
			if strings.Contains(line, input) {
				var lineColored string
				var valInd int
				var colored string
				for j, val := range line {
					if input[valInd] == line[j] {
						colored += string(val)
						if valInd == len(input)-1 {
							lineColored += green(colored)
							valInd = 0
							colored = ""
						} else {
							valInd++
						}

					} else {
						lineColored += colored
						lineColored += string(val)
						valInd = 0
						colored = ""
					}

				}
				lineColored += colored
				results[index] = lineColored
				index++
			}
		}
		endOfSearch(langArgs, results)
	}
}

func endOfSearch(langArgs, results []string) {
	mag := color.New(color.FgMagenta).PrintfFunc()
	blue := color.New(color.FgBlue).PrintfFunc()
	yel := color.New(color.FgYellow).PrintfFunc()
	rc := len(results)
	for i, val := range results {
		mag("*\n--- %v ---\n\n", i+1)
		fmt.Fprintln(color.Output, val)
		blue("\n%s : %v\n", langArgs[7], rc)
		yel("%s : %v", langArgs[8], i+1)
		mag("\n-------------------\n")
		fmt.Println(langArgs[9])
		input := inpuStr()
		if input == "1" {
			break
		}
	}
}
